package util;

import interfaces.User;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserPool {

    private List <User> users;

    public UserPool () {
        users = new ArrayList<>();
    }

    public UserPool (List <User> users) {
        this.users = users;
    }

    public List <User> getUsers() {
        return users;
    }

    public void setUsers(List <User> users) {
        this.users = users;
    }

    public void addUser(User user) {
        users.add(user);
    }

    public User getClientByUserName(String UserName){
        User user = null ;
        for (int i = 0; i < users.size();i++){
            if (users.get(i).getUsername().equals(UserName)) user = users.get(i);
        }
        return user;
    }

    public List getListOrderedByFio (){
        int min = 0;
        List <User> resList = users;
        User tmpUser = null;
        for (int i = 0; i < users.size() - 1;i++){
            min = i;
            for (int k = i+1; k < users.size();k++){
                if (users.get(k).getFio().compareTo(users.get(min).getFio()) < 0 ) {
                    min = k;
                }
            }
            tmpUser = users.get(min);
            resList.set(min, users.get(i));
            resList.set(i, tmpUser);
        }
        return resList;
    }

    public List getListOrderedByBornDate(){
        int min = 0;
        List <User> resList = users;
        User tmpUser = null;
        for (int i = 0; i < users.size() - 1;i++){
           min = i;
           for (int k = i+1; k < users.size();k++){
               min = users.get(k).getBornDate().before(users.get(min).getBornDate()) ? k : min;
           }
            tmpUser = users.get(min);
            resList.set(min, users.get(i));
            resList.set(i, tmpUser);
        }
        return resList;
    }

    public int size(){
        return users.size();
    }
}
