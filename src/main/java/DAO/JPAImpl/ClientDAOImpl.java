package DAO.JPAImpl;

import DAO.ClientDAO;
import entities.Client;

import java.sql.SQLException;
import java.util.List;

public class ClientDAOImpl implements ClientDAO {
    @Override
    public void addClient(Client client) throws SQLException {

    }

    @Override
    public List<Client> getClients() throws SQLException {
        return null;
    }

    @Override
    public Client getClient(int clientId) throws SQLException {
        return null;
    }

    @Override
    public void setClient(Client client) throws SQLException {

    }

    @Override
    public void deleteClient(Client client) throws SQLException {

    }

    @Override
    public void deleteClient(int clientId) throws SQLException {

    }
}
