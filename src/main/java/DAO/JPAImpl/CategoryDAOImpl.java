package DAO.JPAImpl;

import DAO.CategoryDAO;
import entities.Category;

import java.sql.SQLException;
import java.util.List;

public class CategoryDAOImpl implements CategoryDAO {
    @Override
    public void addCategory(Category category) throws SQLException {

    }

    @Override
    public List<Category> getCategories() throws SQLException {
        return null;
    }

    @Override
    public Category getCategory(int categoryId) throws SQLException {
        return null;
    }

    @Override
    public void setCategory(Category category) throws SQLException {

    }

    @Override
    public void deleteCategory(Category category) throws SQLException {

    }

    @Override
    public void deleteCategory(int categoryId) throws SQLException {

    }
}
