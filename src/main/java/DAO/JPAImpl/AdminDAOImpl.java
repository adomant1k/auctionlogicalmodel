package DAO.JPAImpl;

import DAO.AdminDAO;
import entities.Admin;

import java.sql.SQLException;
import java.util.List;

public class AdminDAOImpl implements AdminDAO {
    @Override
    public void addAdmin(Admin admin) throws SQLException {

    }

    @Override
    public List<Admin> getAdmins() throws SQLException {
        return null;
    }

    @Override
    public Admin getAdmin(int adminId) throws SQLException {
        return null;
    }

    @Override
    public void setAdmin(Admin admin) throws SQLException {

    }

    @Override
    public void deleteAdmin(Admin admin) throws SQLException {

    }

    @Override
    public void deleteAdmin(int adminId) throws SQLException {

    }
}
