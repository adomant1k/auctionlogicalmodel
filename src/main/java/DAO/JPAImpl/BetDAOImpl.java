package DAO.JPAImpl;

import DAO.BetDAO;
import entities.Bet;

import java.sql.SQLException;
import java.util.List;

public class BetDAOImpl implements BetDAO {
    @Override
    public void addBet(Bet bet) throws SQLException {

    }

    @Override
    public List<Bet> getBets() throws SQLException {
        return null;
    }

    @Override
    public Bet getBet(int betId) throws SQLException {
        return null;
    }

    @Override
    public void setBet(Bet bet) throws SQLException {

    }

    @Override
    public void deleteBet(Bet bet) throws SQLException {

    }

    @Override
    public void deleteBet(int betId) throws SQLException {

    }
}
