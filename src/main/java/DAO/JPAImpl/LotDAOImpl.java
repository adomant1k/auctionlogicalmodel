package DAO.JPAImpl;

import DAO.LotDAO;
import entities.Lot;

import java.sql.SQLException;
import java.util.List;

public class LotDAOImpl implements LotDAO {
    @Override
    public void addLot(Lot lot) throws SQLException {

    }

    @Override
    public List<Lot> getLots() throws SQLException {
        return null;
    }

    @Override
    public Lot getLot(int lotId) throws SQLException {
        return null;
    }

    @Override
    public void setLot(Lot lot) throws SQLException {

    }

    @Override
    public void deleteLot(Lot lot) throws SQLException {

    }

    @Override
    public void deleteLot(int lotId) throws SQLException {

    }
}
