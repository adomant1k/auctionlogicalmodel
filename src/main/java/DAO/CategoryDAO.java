package DAO;

import entities.Category;
import java.sql.SQLException;
import java.util.List;

public interface CategoryDAO {

    void addCategory (Category category) throws SQLException;

    List<Category> getCategories() throws SQLException;

    Category getCategory (int categoryId) throws SQLException;

    void setCategory (Category category) throws SQLException;

    void deleteCategory (Category category) throws SQLException;

    void deleteCategory (int categoryId) throws SQLException;
}
