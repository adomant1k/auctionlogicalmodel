package DAO.JDBCImpl;

import DAO.CategoryDAO;
import DAO.DAO;
import entities.Category;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CategoryDAOImpl implements CategoryDAO {
    private DAO dao;

    public CategoryDAOImpl(DAO dao){
        this.dao = dao;
    }

    @Override
    public void addCategory(Category category) throws SQLException {
        String query = "INSERT INTO public.\"Category\"" +
                " (categoryId, categoryName)" +
                " VALUES (?,?)";
        try (Connection connection = dao.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, category.getCategoryId());
            preparedStatement.setString(2, category.getCategoryName());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<Category> getCategories() throws SQLException {
        List <Category> categories = new ArrayList<>();
        String query = "SELECT * FROM public.\"Category\"";
        try (Connection connection = dao.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int categoryId = resultSet.getInt("categoryId");
                String categoryName = resultSet.getString("categoryName");
                Category category = new Category(categoryId, categoryName);
                categories.add(category);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return categories;
    }

    @Override
    public Category getCategory(int categoryId) throws SQLException {
        Category category = null;
        String query = "SELECT * FROM public.\"Category\" WHERE categoryId = ?";
        try (Connection connection = dao.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, categoryId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int categoryId_ = resultSet.getInt("categoryId");
                String categoryName = resultSet.getString("categoryName");
                category = new Category(categoryId_, categoryName);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return category;
    }

    @Override
    public void setCategory(Category category) throws SQLException {
        String query = "UPDATE public.\"Category\" SET categoryName = ? WHERE categoryId = ?";
        try (Connection connection = dao.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, category.getCategoryName());
            preparedStatement.setInt(2, category.getCategoryId());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteCategory(Category category) throws SQLException {
        String query = "DELETE FROM public.\"Category\" WHERE categoryId = ?";
        try (Connection connection = dao.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, category.getCategoryId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteCategory(int categoryId) throws SQLException {
        String query = "DELETE FROM public.\"Client\" WHERE categoryId = ?";
        try (Connection connection = dao.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, categoryId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
