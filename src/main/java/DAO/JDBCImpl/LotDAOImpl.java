package DAO.JDBCImpl;

import DAO.DAO;
import DAO.LotDAO;
import entities.Lot;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LotDAOImpl implements LotDAO {
    private DAO dao;

    public LotDAOImpl(DAO dao) {
        this.dao = dao;
    }
    @Override
    public void addLot(Lot lot) throws SQLException {
        String query = "INSERT INTO public.\"Lot\"" +
                " (lotId, lotName, sellerId, startingPrice, redemptionPrice)" +
                " VALUES (?,?,?,?,?)";
        try (Connection connection = dao.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, lot.getLotId());
            preparedStatement.setString(2, lot.getLotName());
            preparedStatement.setInt(3, lot.getSellerId());
            preparedStatement.setString(4, lot.getStartingPrice());
            preparedStatement.setString(5, lot.getRedemptionPrice());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Lot> getLots() throws SQLException {
        List <Lot> lots = new ArrayList<>();
        String query = "SELECT * FROM public.\"Lot\"";
        try (Connection connection = dao.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int lotId = resultSet.getInt("lotId");
                String lotName = resultSet.getString("lotName");
                int sellerId = resultSet.getInt("sellerId");
                String startingPrice = resultSet.getString("startingPrice");
                String redemptionPrice = resultSet.getString("redemptionPrice");
                Lot lot = new Lot(lotId, lotName, sellerId, startingPrice, redemptionPrice);
                lots.add(lot);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lots;
    }

    @Override
    public Lot getLot(int lotId) throws SQLException {
        Lot lot = null;
        String query = "SELECT * FROM public.\"Lot\" WHERE lotId = ?";
        try (Connection connection = dao.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, lotId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int lotId_ = resultSet.getInt("lotId");
                String lotName = resultSet.getString("lotName");
                int sellerId = resultSet.getInt("sellerId");
                String startingPrice = resultSet.getString("startingPrice");
                String redemptionPrice = resultSet.getString("redemptionPrice");
                lot = new Lot(lotId_, lotName, sellerId, startingPrice, redemptionPrice);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lot;
    }

    @Override
    public void setLot(Lot lot) throws SQLException {
        String query = "UPDATE public.\"Lot\" SET lotName = ? , sellerId = ? , " +
                "startingPrice = ? , redemptionPrice = ? WHERE lotId = ?";
        try (Connection connection = dao.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, lot.getLotName());
            preparedStatement.setInt(2, lot.getSellerId());
            preparedStatement.setString(3, lot.getStartingPrice());
            preparedStatement.setString(4, lot.getRedemptionPrice());
            preparedStatement.setInt(5, lot.getLotId());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteLot(Lot lot) throws SQLException {
        String query = "DELETE FROM public.\"Lot\" WHERE lotId = ?";
        try (Connection connection = dao.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, lot.getLotId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteLot(int lotId) throws SQLException {
        String query = "DELETE FROM public.\"Lot\" WHERE lotId = ?";
        try (Connection connection = dao.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, lotId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
