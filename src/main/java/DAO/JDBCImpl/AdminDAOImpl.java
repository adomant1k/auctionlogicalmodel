package DAO.JDBCImpl;

import DAO.AdminDAO;
import DAO.DAO;
import entities.Admin;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AdminDAOImpl implements AdminDAO {
    private DAO dao;

    public AdminDAOImpl(DAO dao){
        this.dao = dao;
    }

    @Override
    public void addAdmin(Admin admin) throws SQLException {
        String query = "INSERT INTO public.\"Admin\"" +
                " (adminId, adminUserName, adminPassword, adminFio, adminBornDate)" +
                " VALUES (?,?,?,?,?)";
        try (Connection connection = dao.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, admin.getId());
            preparedStatement.setString(2, admin.getUsername() );
            preparedStatement.setString(3, admin.getPassword());
            preparedStatement.setString(4, admin.getFio());
            preparedStatement.setDate(5, admin.getBornDate());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Admin> getAdmins() throws SQLException {
        List <Admin> admins = new ArrayList<>();
        String query = "SELECT * FROM public.\"Admin\"";
        try (Connection connection = dao.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int adminId = resultSet.getInt("adminId");
                String adminUserName = resultSet.getString("adminUserName");
                String adminPassword = resultSet.getString("adminPassword");
                String adminFio = resultSet.getString("adminFio");
                Date adminBornDate = resultSet.getDate("adminBornDate");
                Admin admin = new Admin(adminId,adminUserName, adminPassword, adminFio, adminBornDate, false);
                admins.add(admin);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return admins;
    }

    @Override
    public Admin getAdmin(int adminId) throws SQLException {
        Admin admin = null;
        String query = "SELECT * FROM public.\"Admin\" WHERE adminId = ?";
        try (Connection connection = dao.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, adminId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int _adminId_ = resultSet.getInt("adminId");
                String adminUserName = resultSet.getString("adminUserName");
                String adminPassword = resultSet.getString("adminPassword");
                String adminFio = resultSet.getString("adminFio");
                Date adminBornDate = resultSet.getDate("adminBornDate");
                admin = new Admin(_adminId_, adminUserName, adminPassword, adminFio, adminBornDate, false);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return admin;
    }

    @Override
    public void setAdmin(Admin admin) throws SQLException {
        String query = "UPDATE public.\"Admin\" SET adminUserName = ? , adminPassword = ? , " +
                "adminFio = ? , adminBornDate = ? WHERE adminId = ?";
        try (Connection connection = dao.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, admin.getUsername());
            preparedStatement.setString(2, admin.getPassword());
            preparedStatement.setString(3, admin.getFio());
            preparedStatement.setDate(4, admin.getBornDate());
            preparedStatement.setInt(5, admin.getId());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteAdmin(Admin admin) throws SQLException {
        String query = "DELETE FROM public.\"Admin\" WHERE adminId = ?";
        try (Connection connection = dao.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, admin.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteAdmin(int adminId) throws SQLException {
        String query = "DELETE FROM public.\"Admin\" WHERE adminId = ?";
        try (Connection connection = dao.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, adminId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
