package DAO.JDBCImpl;

import DAO.BetDAO;
import DAO.DAO;
import entities.Bet;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BetDAOImpl implements BetDAO {
    private DAO dao;

    public BetDAOImpl(DAO dao) {
        this.dao = dao;
    }
    @Override
    public void addBet(Bet bet) throws SQLException {
        String query = "INSERT INTO public.\"Bet\"" +
                " (betId, betValue, lotId, clientId)" +
                " VALUES (?,?,?,?)";
        try (Connection connection = dao.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, bet.getBetId());
            preparedStatement.setString(2, bet.getBetValue());
            preparedStatement.setInt(3, bet.getLotId());
            preparedStatement.setInt(4, bet.getClientId());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Bet> getBets() throws SQLException {
        List <Bet> bets = new ArrayList<>();
        String query = "SELECT * FROM public.\"Bet\"";
        try (Connection connection = dao.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int betId = resultSet.getInt("betId");
                String betValue = resultSet.getString("betValue");
                int lotId = resultSet.getInt("lotId");
                int clientId = resultSet.getInt("clientId");
                Bet bet = new Bet(betId, betValue, lotId, clientId);
                bets.add(bet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bets;
    }

    @Override
    public Bet getBet(int betId) throws SQLException {
        Bet bet = null;
        String query = "SELECT * FROM public.\"Bet\" WHERE betId = ?";
        try (Connection connection = dao.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, betId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int _betId__ = resultSet.getInt("betId");
                String betValue = resultSet.getString("betValue");
                int lotId = resultSet.getInt("lotId");
                int clientId = resultSet.getInt("clientId");
                bet = new Bet(_betId__, betValue, lotId, clientId);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bet;
    }

    @Override
    public void setBet(Bet bet) throws SQLException {
        String query = "UPDATE public.\"Bet\" SET betValue = ? , lotId = ? , " +
                "clientId = ? WHERE betId = ?";
        try (Connection connection = dao.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, bet.getBetValue());
            preparedStatement.setInt(2, bet.getLotId());
            preparedStatement.setInt(3, bet.getClientId());
            preparedStatement.setInt(4, bet.getBetId());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteBet(Bet bet) throws SQLException {
        String query = "DELETE FROM public.\"Bet\" WHERE betId = ?";
        try (Connection connection = dao.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, bet.getBetId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteBet(int betId) throws SQLException {
        String query = "DELETE FROM public.\"Bet\" WHERE betId = ?";
        try (Connection connection = dao.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, betId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
