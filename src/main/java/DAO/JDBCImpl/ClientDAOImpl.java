package DAO.JDBCImpl;

import DAO.ClientDAO;
import DAO.DAO;
import entities.Client;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ClientDAOImpl implements ClientDAO {
    private DAO dao;

    public ClientDAOImpl(DAO dao) {
        this.dao = dao;
    }
    @Override
    public void addClient(Client client) throws SQLException {
        String query = "INSERT INTO public.\"Client\"" +
                " (clientId, clientUsername, clientPassword, clientFio, clientBornDate)" +
                " VALUES (?,?,?,?,?)";
        try (Connection connection = dao.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, client.getId());
            preparedStatement.setString(2, client.getUsername() );
            preparedStatement.setString(3, client.getPassword());
            preparedStatement.setString(4, client.getFio());
            preparedStatement.setDate(5, client.getBornDate());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Client> getClients() throws SQLException {
        List <Client> clients = new ArrayList<>();
        String query = "SELECT * FROM public.\"Client\"";
        try (Connection connection = dao.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int clientId = resultSet.getInt("clientId");
                String clientUsername = resultSet.getString("clientUsername");
                String clientPassword = resultSet.getString("clientPassword");
                String clientFio = resultSet.getString("clientFio");
                Date clientBornDate = resultSet.getDate("clientBornDate");
                Client client = new Client(clientId, clientUsername, clientPassword, clientFio, clientBornDate);
                clients.add(client);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return clients;
    }

    @Override
    public Client getClient(int clientId) throws SQLException {
        Client client = null;
        String query = "SELECT * FROM public.\"Client\" WHERE clientId = ?";
        try (Connection connection = dao.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, clientId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int _clientId_ = resultSet.getInt("clientId");
                String clientUsername = resultSet.getString("adminUserName");
                String clientPassword = resultSet.getString("adminPassword");
                String clientFio = resultSet.getString("adminFio");
                Date clientBornDate = resultSet.getDate("adminBornDate");
                client = new Client(_clientId_, clientUsername, clientPassword, clientFio, clientBornDate);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return client;
    }

    @Override
    public void setClient(Client client) throws SQLException {
        String query = "UPDATE public.\"Client\" SET clientUsername = ? , clientPassword = ? , " +
                "clientFio = ? , clientBornDate = ? WHERE clientId = ?";
        try (Connection connection = dao.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, client.getUsername());
            preparedStatement.setString(2, client.getPassword());
            preparedStatement.setString(3, client.getFio());
            preparedStatement.setDate(4, client.getBornDate());
            preparedStatement.setInt(5, client.getId());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteClient(Client client) throws SQLException {
        String query = "DELETE FROM public.\"Client\" WHERE clientId = ?";
        try (Connection connection = dao.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, client.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteClient(int clientId) throws SQLException {
        String query = "DELETE FROM public.\"Client\" WHERE clientId = ?";
        try (Connection connection = dao.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, clientId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
