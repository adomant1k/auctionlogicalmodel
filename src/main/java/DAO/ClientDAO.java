package DAO;

import entities.Client;
import java.sql.SQLException;
import java.util.List;

public interface ClientDAO {

    void addClient (Client client) throws SQLException;

    List <Client> getClients() throws SQLException;

    Client getClient (int clientId) throws SQLException;

    void setClient (Client client) throws SQLException;

    void deleteClient (Client client) throws SQLException;

    void deleteClient (int clientId) throws SQLException;
}
