package DAO;

import entities.Guest;
import java.sql.SQLException;
import java.util.List;

public interface GuestDAO {

    void addGuest (Guest guest) throws SQLException;

    List <Guest> getGuests() throws SQLException;

    Guest getGuest (int guestId) throws SQLException;

    void setGuest (Guest guest) throws SQLException;

    void deleteGuest (Guest guest) throws SQLException;

    void deleteGuest (int guestId) throws SQLException;
}
