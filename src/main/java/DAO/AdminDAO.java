package DAO;

import entities.Admin;
import java.sql.SQLException;
import java.util.List;

public interface AdminDAO {

    void addAdmin (Admin admin) throws SQLException;

    List<Admin> getAdmins() throws SQLException;

    Admin getAdmin (int adminId) throws SQLException;

    void setAdmin (Admin admin) throws SQLException;

    void deleteAdmin (Admin admin) throws SQLException;

    void deleteAdmin (int adminId) throws SQLException;
}
