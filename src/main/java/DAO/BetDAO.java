package DAO;

import entities.Bet;
import java.sql.SQLException;
import java.util.List;

public interface BetDAO {

    void addBet (Bet bet) throws SQLException;

    List<Bet> getBets() throws SQLException;

    Bet getBet (int betId) throws SQLException;

    void setBet (Bet bet) throws SQLException;

    void deleteBet (Bet bet) throws SQLException;

    void deleteBet (int betId) throws SQLException;
}
