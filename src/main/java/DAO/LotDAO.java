package DAO;

import entities.Lot;
import java.sql.SQLException;
import java.util.List;

public interface LotDAO {

    void addLot (Lot lot) throws SQLException;

    List <Lot> getLots() throws SQLException;

    Lot getLot (int lotId) throws SQLException;

    void setLot (Lot lot) throws SQLException;

    void deleteLot (Lot lot) throws SQLException;

    void deleteLot (int lotId) throws SQLException;
}
