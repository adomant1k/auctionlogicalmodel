package entities;

import java.util.List;

public class Lot {

    private int lotId;
    private String lotName;
    private int sellerId;
    private String startingPrice;
    private String redemptionPrice;
    private List <Bet> bets;

    public Lot () {

    }

    public Lot (int lotId, String lotName, int sellerId, String startingPrice, String redemptionPrice, List <Bet> bets) {
        this.lotId = lotId;
        this.lotName = lotName;
        this.sellerId = sellerId;
        this.startingPrice = startingPrice;
        this.redemptionPrice = redemptionPrice;
        this.bets = bets;
    }

    public Lot(int lotId, String lotName, int sellerId, String startingPrice, String redemptionPrice) {
        this.lotId = lotId;
        this.lotName = lotName;
        this.sellerId = sellerId;
        this.startingPrice = startingPrice;
        this.redemptionPrice = redemptionPrice;
    }

    public int getLotId() {
        return lotId;
    }

    public String getLotName() {
        return lotName;
    }

    public int getSellerId() {
        return sellerId;
    }

    public String getStartingPrice() {
        return startingPrice;
    }

    public String getRedemptionPrice() {
        return redemptionPrice;
    }

    public List<Bet> getBets() {
        return bets;
    }

    public void setLotId(int lotId) {
        this.lotId = lotId;
    }

    public void setLotName(String lotName) {
        this.lotName = lotName;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public void setStartingPrice(String startingPrice) {
        this.startingPrice = startingPrice;
    }

    public void setRedemptionPrice(String redemptionPrice) {
        this.redemptionPrice = redemptionPrice;
    }

    public void setBets(List<Bet> bets) {
        this.bets = bets;
    }
}
