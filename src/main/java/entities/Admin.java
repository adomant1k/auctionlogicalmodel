package entities;

import interfaces.User;

import java.sql.Date;

public class Admin implements User {


    private int id;

    private String username;

    private String password;

    private String fio;

    private Date bornDate;

    private boolean authenticated;

    public  Admin () {

    }

    public Admin(int id, String username, String password, String fio, Date bornDate, boolean authenticated) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.fio = fio;
        this.bornDate = bornDate;
        this.authenticated = authenticated;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setId(int adminId) {
        this.id = adminId;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }

    public void setAuthenticated() {
        authenticated = true;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public Date getBornDate() {
        return bornDate;
    }

    public void setBornDate(Date bornDate) {
        this.bornDate = bornDate;
    }
}
