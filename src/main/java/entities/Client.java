package entities;

import java.sql.Date;
import java.util.List;
import interfaces.User;

public class Client implements User{

    private int id;

    private String username;

    private String password;

    private String fio;

    private Date bornDate;

    private List<Bet> bets;

    private List<Lot> lots;

    private boolean authenticated;

    public Client () {

    }

    public Client(int id, String username, String password, String fio, Date bornDate, List<Bet> bets, List<Lot> lots, boolean authenticated) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.fio = fio;
        this.bornDate = bornDate;
        this.bets = bets;
        this.lots = lots;
        this.authenticated = authenticated;
    }

    public Client(int id, String username, String password, String fio, Date bornDate) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.fio = fio;
        this.bornDate = bornDate;
        authenticated = false;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public List<Bet> getBets() {
        return bets;
    }

    public List<Lot> getLots() {
        return lots;
    }

    public String getFio() {
        return fio;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setId(int clientId) {
        this.id = clientId;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setBets(List<Bet> bets) {
        this.bets = bets;
    }

    public void setLots(List<Lot> lots) {
        this.lots = lots;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }

    public void setAuthenticated() {
        authenticated = true;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public Date getBornDate() {
        return bornDate;
    }

    public void setBornDate(Date bornDate) {
        this.bornDate = bornDate;
    }
}
