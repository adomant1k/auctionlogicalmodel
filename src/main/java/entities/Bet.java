package entities;

public class Bet {

    private int betId;
    private String betValue;
    private int lotId;
    private int clientId;

    public Bet () {

    }

    public Bet (int betId, String betValue, int lotId, int clientId) {
        this.betId = betId;
        this.betValue = betValue;
        this.lotId = lotId;
        this.clientId = clientId;
    }

    public int getBetId() {
        return betId;
    }

    public String getBetValue() {
        return betValue;
    }

    public int getLotId() {
        return lotId;
    }

    public int getClientId() {
        return clientId;
    }

    public void setBetId(int betId) {
        this.betId = betId;
    }

    public void setBetValue(String betValue) {
        this.betValue = betValue;
    }

    public void setLotId(int lotId) {
        this.lotId = lotId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }
}
