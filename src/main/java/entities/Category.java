package entities;

import java.util.List;

public class Category {

    private int categoryId;
    private String categoryName;
    private List <Lot> lots;

    public Category () {

    }

    public Category(int categoryId, String categoryName, List <Lot> lots){
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.lots = lots;
    }

    public Category(int categoryId, String categoryName) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public List<Lot> getLots() {
        return lots;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public void setLots(List<Lot> lots) {
        this.lots = lots;
    }
}
