package interfaces;


import java.sql.Date;

public interface User {

    int getId();

    void setId(int id);

    String getUsername();

    void setUsername(String username);

    String getPassword();

    String getFio();

    Date getBornDate();

    void setPassword(String password);

    void setFio(String fio);

    void setBornDate(Date bornDate);

    void setAuthenticated(boolean flag);

    void setAuthenticated();
}
