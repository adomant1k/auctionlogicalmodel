import entities.*;
import interfaces.User;
import util.UserPool;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Test {

    public static void main(String [] args) {
        /*
        Создание клиентов, списков их ставок и списков размещенных ими лотов
         */
        Client naruto97 = new Client(1, "naruto97", "qwerty123", "Гусев Денис Игоревич",
                new java.sql.Date (new Date(877377600000L).getTime()), null,null, false);
        List <Bet> naruto97BetsList = new ArrayList <> ();
        List <Lot> naruto97LotsList = new ArrayList<>();

        Client karasik29 = new Client(2, "karasik29", "29101984qw", "Головина Елена Васильевна",
                new java.sql.Date (new Date(542750400000L).getTime()), null,null, false);
        List <Bet> karasik29BetsList = new ArrayList <> ();
        List <Lot> karasik29LotsList = new ArrayList<>();

        Client _xSlayerx_ = new Client(3, "_xSlayerx_", "1337lit", "Детров Пётр Петрович",
                new java.sql.Date (new Date(10353600000L).getTime()), null, null, false);
        List <Bet> _xSlayerx_BetsList = new ArrayList<>();
        List <Lot> _xSlayerx_LotsList = new ArrayList<>();

        UserPool clientPool = new UserPool();
        clientPool.addUser(naruto97);
        clientPool.addUser(karasik29);
        clientPool.addUser(_xSlayerx_);

        Admin admin1 = new Admin(1,"admin", "123", "Иванов Иван Иванович",
                new java.sql.Date (new Date(966555600000L).getTime()), false);
        Admin admin2 = new Admin(2,"newadmin", "123456789", "Копытин Сергей Петрович",
                new java.sql.Date (new Date(777377600000L).getTime()), false);
        UserPool adminPool = new UserPool();
        adminPool.addUser(admin1);
        adminPool.addUser(admin2);



        List <Lot> carsList = new ArrayList <> ();

        Lot audi = new Lot(1,"Audi a8 2018",3, "2000000 Рублей", "25000000 Рублей",null);
        _xSlayerx_LotsList.add(audi);

        List <Bet> audiBetsList = new ArrayList <> ();
        Bet audiBet1 = new Bet(1,"2100000 Рублей", 1,1 );
        naruto97BetsList.add(audiBet1);
        audiBetsList.add(audiBet1);
        Bet audiBet2 = new Bet(2,"2200000 Рублей", 1,2 );
        karasik29BetsList.add(audiBet2);
        audiBetsList.add(audiBet2);
        audi.setBets(audiBetsList);
        carsList.add(audi);

        Lot mercedes = new Lot(2, "Mercedes glk 220 2015", 3, "1500000 Рублей", "20000000 Рублей",null);
        _xSlayerx_LotsList.add(mercedes);

        List <Bet> mercedesBetsList = new ArrayList <> ();
        Bet mercedesBet1 = new Bet(3,"1550000 Рублей",2 , 1);
        naruto97BetsList.add(mercedesBet1);
        mercedesBetsList.add(mercedesBet1);
        Bet mercedesBet2 = new Bet(4,"1600000 Рублей",2 ,2 );
        karasik29BetsList.add(mercedesBet2);
        mercedesBetsList.add(mercedesBet2);
        mercedes.setBets(mercedesBetsList);
        carsList.add(mercedes);

        Lot bmw = new Lot(3, "BMW e528i 1999", 3, "500000 Рублей", "800000 Рублей",null);
        _xSlayerx_LotsList.add(bmw);

        List <Bet> bmwBetsList = new ArrayList<>();
        Bet bmwBet1 = new Bet(5,"510000 Рублей",3, 2);
        karasik29BetsList.add(bmwBet1);
        bmwBetsList.add(bmwBet1);
        Bet bmwBet2 = new Bet(6,"520000 Рублей",3, 1);
        naruto97BetsList.add(bmwBet2);
        bmwBetsList.add(bmwBet2);
        bmw.setBets(bmwBetsList);
        carsList.add(bmw);

        Category cars = new Category(1, "Cars", carsList);


        List <Lot> jewelryList = new ArrayList <> ();

        Lot ring = new Lot(4,"Ring of red gold 585 with cubic zirconia",1, "15000 Рублей", "50000 Рублей",null);
        naruto97LotsList.add(ring);

        List <Bet> ringBetsList = new ArrayList <> ();
        Bet ringBet1 = new Bet(7,"16000 Рублей", 4, 2);
        karasik29BetsList.add(ringBet1);
        ringBetsList.add(ringBet1);
        Bet ringBet2 = new Bet(8,"18000 Рублей", 4,3);
        _xSlayerx_BetsList.add(ringBet2);
        ringBetsList.add(ringBet2);
        ring.setBets(ringBetsList);
        jewelryList.add(ring);

        Lot necklace = new Lot(5, "Pendant from 14 karat red gold with diamond and emerald", 1, "10000 Рублей", "30000 Рублей",null);
        naruto97LotsList.add(necklace);

        List <Bet> necklaceBetsList = new ArrayList <> ();
        Bet necklaceBet1 = new Bet(9,"10500 Рублей", 5,3);
        _xSlayerx_BetsList.add(necklaceBet1);
        necklaceBetsList.add(necklaceBet1);
        Bet necklaceBet2 = new Bet(10, "11000 Рублей", 5, 2);
        karasik29BetsList.add(necklaceBet2);
        necklaceBetsList.add(necklaceBet2);
        necklace.setBets(necklaceBetsList);
        jewelryList.add(necklace);

        Lot earring = new Lot(6, "Earrings from 14 karat red gold with ruby and diamond", 1, "50000 Рублей", "100000 Рублей",null);
        naruto97LotsList.add(earring);

        List <Bet> earringBetsList = new ArrayList <> ();
        Bet earringBet1 = new Bet(11,"55000 Рублей",6,2);
        karasik29BetsList.add(earringBet1);
        earringBetsList.add(earringBet1);
        Bet earringBet2 = new Bet(12,"60000 Рублей",6,3);
        _xSlayerx_BetsList.add(earringBet2);
        earringBetsList.add(earringBet2);
        earring.setBets(earringBetsList);
        jewelryList.add(earring);


        Category jewelry = new Category(2, "Jewelry", jewelryList);

        List <Lot> antiquesList = new ArrayList <> ();

        Lot painting = new Lot(7,"I.K. Aivazovsky \"Fishermen, choosing tackle\" 1892",1, "3000000 Рублей", "5000000 Рублей",null);
        karasik29LotsList.add(painting);

        List <Bet> paintingBetsList = new ArrayList<>();
        Bet paintingBet1 = new Bet(13, "3500000 Рублей", 7, 1);
        naruto97BetsList.add(paintingBet1);
        paintingBetsList.add(paintingBet1);
        Bet paintingBet2 = new Bet(14, "3600000 Рублей", 7, 3);
        _xSlayerx_BetsList.add(paintingBet2);
        paintingBetsList.add(paintingBet2);
        painting.setBets(paintingBetsList);
        antiquesList.add(painting);

        Lot argentum = new Lot(8, "Carl Faberge. Crystal candy in silver. Russian modern \"Flowers of cherry\" beginning 20th century", 1, "250000 Рублей", "700000 Рублей",null);
        karasik29LotsList.add(argentum);

        List <Bet> argentumBetsList = new ArrayList<>();
        Bet argentumBet1 = new Bet(15, "260000 Рублей", 8,1);
        naruto97BetsList.add(argentumBet1);
        argentumBetsList.add(argentumBet1);
        Bet argentumBet2 = new Bet(16, "270000 Рублей",8,3 );
        _xSlayerx_BetsList.add(argentumBet2);
        argentumBetsList.add(argentumBet2);
        argentum.setBets(argentumBetsList);
        antiquesList.add(argentum);

        Lot mirror = new Lot(9, "Antique mirror with console, 19th century.", 1, "30000 Рублей", "1000000 Рублей",null);
        karasik29LotsList.add(mirror);

        List <Bet> mirrorBetsList = new ArrayList<>();
        Bet mirrorBet1 = new Bet(17, "50000 Рублей", 9, 3);
        _xSlayerx_BetsList.add(mirrorBet1);
        mirrorBetsList.add(mirrorBet1);
        Bet mirrorBet2 = new Bet(18, "55000 Рублей", 9, 1);
        naruto97BetsList.add(mirrorBet2);
        mirrorBetsList.add(mirrorBet2);
        mirror.setBets(mirrorBetsList);
        antiquesList.add(mirror);

        Category antiques = new Category(3, "Antiques", antiquesList);

        naruto97.setLots(naruto97LotsList);
        naruto97.setBets(naruto97BetsList);

        karasik29.setLots(karasik29LotsList);
        karasik29.setBets(karasik29BetsList);

        _xSlayerx_.setLots(_xSlayerx_LotsList);
        _xSlayerx_.setBets(_xSlayerx_BetsList);


        Scanner in = new Scanner(System.in);
        System.out.println("Войти как\n1.Гость\n2.Клиент\n3.Администратор");
        switch(Integer.parseInt(in.nextLine())) {
            case 1:
                System.out.println("1.Просмотреть список лотов по категорииям");
                if (Integer.parseInt(in.nextLine()) == 1) {
                    System.out.println("Категории:\n1.Машины\n2.Ювелирные украшения\n3.Антиквариат");
                    switch(Integer.parseInt(in.nextLine())) {
                        case 1:
                            System.out.printf("ID категории: %d\tИмя категории: %s\nЛоты:\n",cars.getCategoryId(),cars.getCategoryName());
                            for (int i = 0;i < carsList.size();i++) {
                                System.out.printf("%d.\t ID лота: %d\tИмя лота: %s\tID продавца: %d\tСтартовая цена: %s\tЦена выкупа: %s\n", i+1,
                                        carsList.get(i).getLotId(), carsList.get(i).getLotName(),carsList.get(i).getSellerId(),
                                        carsList.get(i).getStartingPrice(),carsList.get(i).getRedemptionPrice());
                            }
                            break;
                        case 2:
                            System.out.printf("ID категории: %d\tИмя категории: %s\nЛоты:\n",jewelry.getCategoryId(),jewelry.getCategoryName());
                            for (int i = 0;i < jewelryList.size();i++) {
                                System.out.printf("%d.\tID лота: %d\tИмя лота: %s\tID продавца: %d\tСтартовая цена: %s\tЦена выкупа: %s\n", i+1,
                                        jewelryList.get(i).getLotId(), jewelryList.get(i).getLotName(),jewelryList.get(i).getSellerId(),
                                        jewelryList.get(i).getStartingPrice(),jewelryList.get(i).getRedemptionPrice());
                            }
                            break;
                        case 3:
                            System.out.printf("ID категории: %d\tИмя категории: %s\nЛоты:\n",antiques.getCategoryId(),antiques.getCategoryName());
                            for (int i = 0;i < antiquesList.size();i++) {
                                System.out.printf("%d.\tID лота: %d\tИмя лота: %s\tID продавца: %d\tСтартовая цена: %s\tЦена выкупа: %s\n", i+1,
                                        antiquesList.get(i).getLotId(), antiquesList.get(i).getLotName(),antiquesList.get(i).getSellerId(),
                                        antiquesList.get(i).getStartingPrice(),antiquesList.get(i).getRedemptionPrice());
                            }
                            break;
                        default:
                            System.out.println("Вы нажали не ту кнопку");
                            break;
                    }
                } else System.out.println("Вы нажали не ту кнопку");
                break;
            case 2:
                System.out.println("Для авторизации введите персональные данные\nЛогин:");
                String username = in.nextLine();
                Client tmpclient = (Client) clientPool.getClientByUserName(username);
                if ( tmpclient != null) {
                    System.out.println("Введите пароль");
                    if (tmpclient.getPassword().equals(in.nextLine())) {
                        tmpclient.setAuthenticated();
                        System.out.println("Вы авторизировались как " + username + "\n1.Просмотреть список лотов по категорииям\n2.Просмотреть список лотов размещенных вами\n3.Ваши ставки");
                        switch (Integer.parseInt(in.nextLine())){
                            case 1:
                                System.out.println("Категории:\n1.Машины\n2.Ювелирные украшения\n3.Антиквариат");
                                switch(Integer.parseInt(in.nextLine())) {
                                    case 1:
                                        System.out.printf("ID категории: %d\tИмя категории: %s\nЛоты:\n",cars.getCategoryId(),cars.getCategoryName());
                                        for (int i = 0;i < carsList.size();i++) {
                                            System.out.printf("%d.\tID лота: %d\tИмя лота: %s\tID продавца: %d\tСтартовая цена: %s\tЦена выкупа: %s\n", i+1,
                                                    carsList.get(i).getLotId(), carsList.get(i).getLotName(),carsList.get(i).getSellerId(),
                                                    carsList.get(i).getStartingPrice(),carsList.get(i).getRedemptionPrice());
                                        }
                                        System.out.println("Введите номер строки чтобы перейти к ставкам по лоту");
                                        switch(Integer.parseInt(in.nextLine())) {
                                            case 1:
                                                System.out.printf("Ставки по лоту: %s\n", carsList.get(0).getLotName());
                                                for (int i = 0;i < audiBetsList.size();i++){
                                                    System.out.printf("%d.\tID ставки: %d\tСумма ставки: %s\t ID аукционера: %d\n", i+1, audiBetsList.get(i).getBetId(),
                                                            audiBetsList.get(i).getBetValue(), audiBetsList.get(i).getClientId());}
                                                break;
                                            case 2:
                                                System.out.printf("Ставки по лоту: %s\n", carsList.get(1).getLotName());
                                                for (int i = 0;i < mercedesBetsList.size();i++){
                                                    System.out.printf("%d.\tID ставки: %d\tСумма ставки: %s\t ID аукционера: %d\n", i+1, mercedesBetsList.get(i).getBetId(),
                                                            mercedesBetsList.get(i).getBetValue(), mercedesBetsList.get(i).getClientId());}
                                                break;
                                            case 3:
                                                System.out.printf("Ставки по лоту: %s\n", carsList.get(2).getLotName());
                                                for (int i = 0;i < bmwBetsList.size();i++){
                                                    System.out.printf("%d.\tID ставки: %d\tСумма ставки: %s\t ID аукционера: %d\n",i+1, bmwBetsList.get(i).getBetId(),
                                                            bmwBetsList.get(i).getBetValue(), bmwBetsList.get(i).getClientId());}
                                                break;
                                            default:
                                                System.out.println("Вы нажали не ту кнопку");
                                                break;
                                        }
                                        break;
                                    case 2:
                                        System.out.printf("ID категории: %d\tИмя категории: %s\nЛоты:\n",jewelry.getCategoryId(),jewelry.getCategoryName());
                                        for (int i = 0;i < jewelryList.size();i++) {
                                            System.out.printf("%d.\tID лота: %d\tИмя лота: %s\tID продавца: %d\tСтартовая цена: %s\tЦена выкупа: %s\n", i+1,
                                                    jewelryList.get(i).getLotId(), jewelryList.get(i).getLotName(),jewelryList.get(i).getSellerId(),
                                                    jewelryList.get(i).getStartingPrice(),jewelryList.get(i).getRedemptionPrice());
                                        }
                                        System.out.println("Введите номер строки чтобы перейти к ставкам по лоту");
                                        switch(Integer.parseInt(in.nextLine())) {
                                            case 1:
                                                System.out.printf("Ставки по лоту: %s\n", jewelryList.get(0).getLotName());
                                                for (int i = 0;i < ringBetsList.size();i++){
                                                    System.out.printf("%d.\tID ставки: %d\tСумма ставки: %s\t ID аукционера: %d\n", i+1, ringBetsList.get(i).getBetId(),
                                                            ringBetsList.get(i).getBetValue(), ringBetsList.get(i).getClientId());}
                                                break;
                                            case 2:
                                                System.out.printf("Ставки по лоту: %s\n", jewelryList.get(1).getLotName());
                                                for (int i = 0;i < necklaceBetsList.size();i++){
                                                    System.out.printf("%d.\tID ставки: %d\tСумма ставки: %s\t ID аукционера: %d\n", i+1, necklaceBetsList.get(i).getBetId(),
                                                            necklaceBetsList.get(i).getBetValue(), necklaceBetsList.get(i).getClientId());}
                                                break;
                                            case 3:
                                                System.out.printf("Ставки по лоту: %s\n", jewelryList.get(2).getLotName());
                                                for (int i = 0;i < earringBetsList.size();i++){
                                                    System.out.printf("%d.\tID ставки: %d\tСумма ставки: %s\t ID аукционера: %d\n", i+1, earringBetsList.get(i).getBetId(),
                                                            earringBetsList.get(i).getBetValue(), earringBetsList.get(i).getClientId());}
                                                break;
                                            default:
                                                System.out.println("Вы нажали не ту кнопку");
                                                break;
                                        }
                                        break;
                                    case 3:
                                        System.out.printf("ID категории: %d\tИмя категории: %s\nЛоты:\n",antiques.getCategoryId(),antiques.getCategoryName());
                                        for (int i = 0;i < antiquesList.size();i++) {
                                            System.out.printf("%d.\tID лота: %d\tИмя лота: %s\tID продавца: %d\tСтартовая цена: %s\tЦена выкупа: %s\n", i+1,
                                                    antiquesList.get(i).getLotId(), antiquesList.get(i).getLotName(),antiquesList.get(i).getSellerId(),
                                                    antiquesList.get(i).getStartingPrice(),antiquesList.get(i).getRedemptionPrice());
                                        }
                                        System.out.println("Введите номер строки чтобы перейти к ставкам по лоту");
                                        switch(Integer.parseInt(in.nextLine())) {
                                            case 7:
                                                System.out.printf("Ставки по лоту: %s\n", antiquesList.get(0).getLotName());
                                                for (int i = 0;i < paintingBetsList.size();i++){
                                                    System.out.printf("%d.\tID ставки: %d\tСумма ставки: %s\t ID аукционера: %d\n", i+1, paintingBetsList.get(i).getBetId(),
                                                            paintingBetsList.get(i).getBetValue(),paintingBetsList.get(i).getClientId());}
                                                break;
                                            case 8:
                                                System.out.printf("Ставки по лоту: %s\n", antiquesList.get(1).getLotName());
                                                for (int i = 0;i < argentumBetsList.size();i++){
                                                    System.out.printf("%d.\tID ставки: %d\tСумма ставки: %s\t ID аукционера: %d\n", i+1, argentumBetsList.get(i).getBetId(),
                                                            argentumBetsList.get(i).getBetValue(), argentumBetsList.get(i).getClientId());}
                                                break;
                                            case 9:
                                                System.out.printf("Ставки по лоту: %s\n", antiquesList.get(2).getLotName());
                                                for (int i = 0;i < mirrorBetsList.size();i++){
                                                    System.out.printf("%d.\tID ставки: %d\tСумма ставки: %s\t ID аукционера: %d\n", i+1, mirrorBetsList.get(i).getBetId(),
                                                            mirrorBetsList.get(i).getBetValue(), mirrorBetsList.get(i).getClientId());}
                                                break;
                                            default:
                                                System.out.println("Вы нажали не ту кнопку");
                                                break;
                                        }
                                        break;
                                    default:
                                        System.out.println("Вы нажали не ту кнопку");
                                        break;
                                }
                                break;
                            case 2:
                                System.out.println("Лоты размещенные вами:");
                                List <Lot> tmpLotsList = tmpclient.getLots();
                                for (int i = 0 ; i < tmpLotsList.size();i++) {
                                    System.out.printf("%d.\tID лота: %d\tИмя лота: %s\tID продавца: %d\tСтартовая цена: %s\tЦена выкупа: %s\n", i+1,
                                            tmpLotsList.get(i).getLotId(), tmpLotsList.get(i).getLotName(),tmpLotsList.get(i).getSellerId(),
                                            tmpLotsList.get(i).getStartingPrice(),tmpLotsList.get(i).getRedemptionPrice());
                                }
                                System.out.println("Введите номер строки, чтобы просмотреть ставки по лоту");
                                switch (Integer.parseInt(in.nextLine())) {
                                    case 1:
                                        System.out.printf("Ставки по лоту: %s\n", tmpLotsList.get(0).getLotName());
                                        for (int i = 0; i < tmpLotsList.get(0).getBets().size(); i++) {
                                            System.out.printf("%d.\tID ставки: %d\tСумма ставки: %s\t ID аукционера: %d\n", i + 1, tmpLotsList.get(0).getBets().get(i).getBetId(),
                                                    tmpLotsList.get(0).getBets().get(i).getBetValue(), tmpLotsList.get(0).getBets().get(i).getClientId());
                                        }
                                        break;
                                    case 2:
                                        System.out.printf("Ставки по лоту: %s\n", tmpLotsList.get(1).getLotName());
                                        for (int i = 0; i < tmpLotsList.get(1).getBets().size(); i++) {
                                            System.out.printf("%d.\tID ставки: %d\tСумма ставки: %s\t ID аукционера: %d\n", i + 1, tmpLotsList.get(1).getBets().get(i).getBetId(),
                                                    tmpLotsList.get(1).getBets().get(i).getBetValue(), tmpLotsList.get(1).getBets().get(i).getClientId());
                                        }
                                        break;
                                    case 3:
                                        System.out.printf("Ставки по лоту: %s\n", tmpLotsList.get(2).getLotName());
                                        for (int i = 0; i < tmpLotsList.get(2).getBets().size(); i++) {
                                            System.out.printf("%d.\tID ставки: %d\tСумма ставки: %s\t ID аукционера: %d\n", i + 1, tmpLotsList.get(2).getBets().get(i).getBetId(),
                                                    tmpLotsList.get(2).getBets().get(i).getBetValue(), tmpLotsList.get(2).getBets().get(i).getClientId());
                                        }
                                        break;
                                    default:
                                        System.out.println("Вы нажали не ту кнопку");
                                        break;
                                }
                            case 3:
                                System.out.println("Ваши ставки:");
                                for (int i = 0; i < tmpclient.getBets().size(); i++) {
                                    System.out.printf("%d.\tID ставки: %d\tСумма ставки: %s\n", i + 1, tmpclient.getBets().get(i).getBetId(),
                                            tmpclient.getBets().get(i).getBetValue());
                                }
                                break;
                            default:
                                break;
                        }
                    } else System.out.println("Вы ввели неправильный пароль");
                } else System.out.println("Пользователя с логином " + username + " не существует");
                break;
            case 3:
                System.out.println("Для авторизации введите персональные данные\nЛогин:");
                String username_ = in.nextLine();
                Admin tmpadmin = (Admin) adminPool.getClientByUserName(username_);
                if ( tmpadmin != null) {
                    System.out.println("Введите пароль");
                    if (tmpadmin.getPassword().equals(in.nextLine())) {
                        tmpadmin.setAuthenticated();
                        System.out.println("Вы авторизировались как " + username_ + "\n1.Просмотреть статистику по пользователям\n2.Получить список пользователей отсортированный по ФИО" +
                                "\n3.Получить список пользователей отсортированный по дате рождения");
                        switch (Integer.parseInt(in.nextLine())) {
                            case 1:
                                for (int i = 0; i < clientPool.size();i++) {
                                    Client tmp = (Client) clientPool.getUsers().get(i);
                                    System.out.printf("Пользователь: %s\tКоличество лотов %d\tКоличество ставок: %d\n", tmp.getUsername(),
                                            tmp.getLots().size(), tmp.getBets().size());
                                }
                                break;
                            case 2:
                                List <User> listOrderedByFio = clientPool.getListOrderedByFio();
                                for (User cl : listOrderedByFio){
                                    System.out.printf("Пользователь: %s\tФИО: %s\tДата рождения: %tF\n", cl.getUsername(),
                                            cl.getFio(), cl.getBornDate());
                                }
                                break;
                            case 3:
                                List <User> listOrderedByBornDate = clientPool.getListOrderedByBornDate();
                                for (User cl : listOrderedByBornDate){
                                    System.out.printf("Пользователь: %s\tФИО: %s\tДата рождения: %tF\n", cl.getUsername(),
                                            cl.getFio(), cl.getBornDate());
                                }
                                break;
                            default:
                                    break;
                        }
                    } else System.out.println("Вы ввели неправильный пароль");
                } else System.out.println("Пользователя с логином " + username_ + " не существует");
                break;
            default:
                System.out.println("Вы нажали не ту кнопку\n");
                break;
        }
    }
}
